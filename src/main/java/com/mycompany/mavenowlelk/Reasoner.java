/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenowlelk;

/**
 *
 * @author KemasRahmatSalehWiharja
 */
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.util.InferredAxiomGenerator;
import org.semanticweb.owlapi.util.InferredOntologyGenerator;
import org.semanticweb.owlapi.util.InferredSubClassAxiomGenerator;
import org.semanticweb.owlapi.util.InferredEquivalentClassAxiomGenerator;

public class Reasoner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       
        OWLOntologyManager managerIn = OWLManager.createOWLOntologyManager();
        PrintStream out = null;
        String logFolder = "log/";
        boolean writeLog = true;
        //OWLOntologyManager managerOut = OWLManager.createOWLOntologyManager();
        try{
            
            File logDirectory = new File(logFolder);
            if(!logDirectory.exists())
                logDirectory.mkdir();
            out = new PrintStream(new FileOutputStream(logFolder + "GEL output_" + getCurrentTimeStamp() + ".txt"));
            if(writeLog)
                System.setOut(out);
            
            OWLOntology ont = true ? managerIn.loadOntologyFromOntologyDocument(new File ("data/pizza.owl"))
            : managerIn.loadOntologyFromOntologyDocument(IRI.create("data/pizza.owl"));
            System.out.println("The Ontology : "+ont.getAxiomCount());
            //OWLOntology ontOut = true ? managerOut.loadOntologyFromOntologyDocument(new File ("/result/pizzaO.owl"))
            //: managerOut.loadOntologyFromOntologyDocument(IRI.create("/result/pizzaO.owl"));
            //OWLOntology ontOut = null;
            OWLReasonerFactory reasonerFactory = new ElkReasonerFactory();
            OWLReasoner reasoner = reasonerFactory.createReasoner(ont);
            
            // Classify the ontology.
            reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
            // To generate an inferred ontology we use implementations of
            // inferred axiom generators
            List<InferredAxiomGenerator<? extends OWLAxiom>> gens = new ArrayList<InferredAxiomGenerator<? extends OWLAxiom>>();
            gens.add(new InferredSubClassAxiomGenerator());
            gens.add(new InferredEquivalentClassAxiomGenerator());

            // Put the inferred axioms into a fresh empty ontology.
            OWLOntology infOnt = managerIn.createOntology();
            InferredOntologyGenerator iog = new InferredOntologyGenerator(reasoner,gens);
            iog.fillOntology(managerIn, infOnt);

            // Save the inferred ontology.
            //managerOut.saveOntology(infOnt,OWLOntologyFormat(),
            //IRI.create((new File("path to result").toURI())));
            
            managerIn.saveOntology(infOnt, IRI.create(new File("data/hasil3.owl").toURI()));
    // Terminate the worker threads used by the reasoner.
            reasoner.dispose();
            out.close();           
        }catch (OWLOntologyCreationException ex) 
        {out.close();
        }catch (FileNotFoundException ex) 
        {
            out.close();
        }catch (OWLOntologyStorageException ex) 
        {
            ex.printStackTrace();
        }
        //panggil reasoner
    }
  public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }   
}
